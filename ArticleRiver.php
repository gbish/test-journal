<?php

use GuzzleHttp\Client;

Class ArticleRiver {

	private $client;
	private $publication;

	private $apiBase = "https://api.thejournal.ie/v3/sample/";
	private $apiAuth = ['sample', 'eferw5wr335£65'];

	public $articles;
	public $pagination;

	public function __construct() {
		$this->client = new Client();		// Create Guzzle Client
		$this->setPublication();			// Set Default Publication
	}


	// -- Return Articles
	public function getAllArticles() {

		// Get Response
		$this->generateRequest($this->publication); 

	}

	// -- Return Tagged Articles
	public function getTaggedArticles($tag=""){

		// Check for no tag
		if(empty($tag)) { return []; }

		// Validate Tag (Spaces to dashes)
		$tag = str_replace(" ", "-", $tag);

		// Get Response
		$this->generateRequest('tag/'.$tag); 

	}

	// -- Set Requested Publication
	public function setPublication($publication="") {
		$this->publication = $publication;
	}


	// -- Return individual Article Elements
	public function getArticleElement($articleItem, $element) {

		// id, title, excerpt...
		switch($element) {
			case "title":
				return $articleItem->title;
			case "excerpt":
				return $articleItem->excerpt;
			case "riverimage":
				return $articleItem->images->river_0->image;
		}


	}

	// -- Listing Rules -> Limit to "post" only
	public function listingRules($articleItem) {

		return (strtolower($articleItem->type) === "post") ? true : false;

	}



// - Helpers ------------------------------------------------------------------------

	// -- Process Response 
	private function processResponse($response) {

		// Check Response = OK
		if(intval($response->getStatusCode()) === 200) {

			$data = $this->decodeBodyJson($response->getBody());

			$this->articles = $data->response->articles;
			$this->pagination = $data->response->pagination;

		}

		// Otherwise Fail (& etc..)
		else {

			$this->articles = [];
			$this->pagination = [];

		}

	}

	// -- Guzzle Request & Process
	private function generateRequest($path="") {

		$this->processResponse($this->client->request('GET', $this->apiBase.$path, [
		    'auth' => $this->apiAuth
		]));

	}

	// -- Decode Server Body Response
	private function decodeBodyJson($string="") {
		return (!empty($string)) ? json_decode($string) : [];
	}

}