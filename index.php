<?php

require 'vendor/autoload.php';
require 'ArticleRiver.php';


$ArticleRiver = new ArticleRiver();

if(!empty($_GET['tag'])) {

	// Get Tagged Articles
	$ArticleRiver->getTaggedArticles($_GET['tag']);

} else {

	// Get Articles
	$ArticleRiver->getAllArticles();

}


?><!DOCTYPE html>
<html>
<head>
	<title>Graham Bishop - Journal 2017</title>
</head>
<body>

<?php 
// Output TAG if tagged items only
if(!empty($_GET['tag'])) { ?>
	<h2>Items tagged &quot;<?php echo $_GET['tag']; ?>&quot;</h2>
<?php } ?>

<ul>
<?php 
	foreach($ArticleRiver->articles AS $item ) {

		// List only "posts"
		if(!$ArticleRiver->listingRules($item)) {
			continue;
		}

		// Output Contents
		echo '<li>';
		echo '<h3>'.$ArticleRiver->getArticleElement($item, "title").'</h3>';
		echo '<img src="'.$ArticleRiver->getArticleElement($item, "riverimage").'" style="float: left;">';
		echo '<p>'.$ArticleRiver->getArticleElement($item, "excerpt").'</p>';
		echo '</li>';
	}
?>
</ul>

</body>
</html>

